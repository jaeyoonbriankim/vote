<?php
$conn = mysqli_connect("us-cdbr-azure-east-c.cloudapp.net","b7372d6f42893e","687f794f","votedb");

if (mysqli_connect_errno())
{
  echo "Connection failed: %s\n", mysqli_connect_errno();
}

$get_votes = "SELECT * FROM votes";
$run_votes = mysqli_query($conn, $get_votes);
$row_votes = mysqli_fetch_array($run_votes);


$yes = $row_votes["yes"];
$no = $row_votes["no"];
$tot = $yes+$no;

mysqli_close($conn);
?>

<!DOCTYPE html>
<html lang="en">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko"  xml:lang="ko">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="dcterms.created" content="Mon, 18 Apr 2016 17:20:21 GMT">
    <meta name="description" content="Voting page for KISEM">
    <meta name="keywords" content="KISEM">
	
    <title>KISEM Vote</title>
    
    <style type="text/css">
    <!--
    body {
      color:#002878;
      background-color:#FFFFFF;
    }
    a  { color:#0000FF; }
    a:visited { color:#800080; }
    a:hover { color:#008000; }
    a:active { color:#FF0000; }
	h1 {
	   font-size:50px; 
	}
	.footer, .push {
	  position:absolute;
	  width: 100%;
      margin-bottom:0;
	  background-color:black;
	  text-align:center;
	  padding-top:40px;
	  color: #cc8a00;
	  height: 100px;
	  border-top: 10px solid #cc8a00;
	}
	.yesbtn{
    	border : solid 1px #ffffff;
    	border-radius : 3px;
    	moz-border-radius : 3px;
    	-webkit-box-shadow : 0px 0px 30px #cc8a00;
    	-moz-box-shadow : 0px 0px 30px #cc8a00;
    	box-shadow : 0px 0px 30px #cc8a00;
    	font-size : 20px;
    	color : #ffdf00;
    	padding : 10px 20px;
    	background-color : #002878;
    	cursor:pointer;
	}
	.yesbtn:hover {
		opacity:0.8;
	}
	
	.nobtn{
    	border : solid 1px #ffffff;
    	border-radius : 3px;
    	moz-border-radius : 3px;
    	-webkit-box-shadow : 0px 0px 30px #cc8a00;
    	-moz-box-shadow : 0px 0px 30px #cc8a00;
    	box-shadow : 0px 0px 30px #cc8a00;
    	font-size : 20px;
    	color : #ffdf00;
    	padding : 10px 23px;
    	background-color : #002878;
    	cursor:pointer;
	}
	.nobtn:hover {
		opacity:0.8;
	}
    -->
    </style>
    <!--[if IE]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
  
  <body>
  	<p style="text-align:center">
  	   <img src="https://reetzyplusimg.blob.core.windows.net/pub/kisem_logo.jpg" width="960" height="301" alt="" border="0">
	</p>
	<hr>
    <h1 style="text-align:center">KISEM Presidential Vote</h1>
    <h2 style="text-align:center; font-size:50px; font-style: oblique;">#1 Han Kyu Lee</h2>
    <p style="max-width:1280px; margin: 0 auto !important; padding-bottom:30px;"><img src="https://reetzyplusimg.blob.core.windows.net/pub/hankyu_speech.PNG" alt="Hankyu Speech" style='width:100%;' border="0"></p>
	<hr>

	<br>
	
	<p style="text-align:center; font-size:40px;" >Do you support <b><i>Han Kyu Lee</i></b>?</p>
	<div class ="container" style="text-align:center">
    	<form action="vote.php" method="POST">
    	<input type= "submit" name="yes" class="yesbtn" value="YES" />
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    	<input type= "submit" name="no" class="nobtn" value="NO" />
    	</form>
	</div>
	<br>
  <br>
  <p style="text-align:center; font-size:27px;">Current Vote Status</p>
  <p style="text-align:center; font-size:25px; padding-bottom:2px"><b>YES</b>: <?php echo $yes ?>&nbsp&nbsp&nbsp&nbsp<b>NO</b>: <?php echo $no ?></p>         
  <p style="text-align:center; font-size:25px; color:#cc8a00">Total: <?php echo $tot ?></p>
	<br>
  <br>
  <p style="text-align:center; font-size:20px; color:red">**Please do not vote more than once. Your IP address is being checked.</p>
  <p style="text-align:center; font-size:20px; color:red"><?php echo "Your IP is ".$_SERVER["REMOTE_ADDR"]; ?></p>
	<br>
	<br>
  </body>
  
  <div class="footer">
  	   <p>&copy; 2016 Jaeyoon (Brian) Kim. All Rights Reserved.</p>
  </div>
  
</html>




