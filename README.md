A simple yes or no voting system for a club wide presidential vote.

Used PHP, HTML, CSS to create the webpage and used MySQL as the database containing "yes" and "no" columns.

Sends the votes and the IP address of the voters to my email with an SMTP server.

Used Microsoft Azure for the Server and the Sendgrid SMTP.