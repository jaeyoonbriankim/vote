<?php
$conn = mysqli_connect("database hostname","username","password","database name");
$check = 0;
if (mysqli_connect_errno())
{
	echo "Connection failed: %s\n", mysqli_connect_errno();
}

if(isset($_POST['yes']))
{
  $check = 1;
	$vote_yes = "UPDATE votes SET yes=yes+1";
	$run_yes = mysqli_query($conn, $vote_yes);
}

if(isset($_POST['no']))
{
  $check = 2;
	$vote_no = "UPDATE votes SET no=no+1";
	$run_no = mysqli_query($conn, $vote_no);	
}

$get_votes = "SELECT * FROM votes";
$run_votes = mysqli_query($conn, $get_votes);
$row_votes = mysqli_fetch_array($run_votes);


$yes = $row_votes["yes"];
$no = $row_votes["no"];
$tot = $yes+$no;
$ipip = $_SERVER["REMOTE_ADDR"]; 

mysqli_close($conn);
?>
<?php 
$errors = '';
$myemail = 'myemail@email.com';

$email_address = 'fromemail@email.com'; 

if(empty($errors))
{
  $url = 'https://api.sendgrid.com/';
  $user = 'sendgrid username';
  $pass = 'sendgrid password'; 

  $params = array(
       'api_user' => $user,
       'api_key' => $pass,
       'to' => $myemail,
       'subject' => 'vote ips',
       'html' => 'good '.$ipip.'   vote '.$check,
       'text' => 'good',
       'from' => $email_address,
    );

  $request = $url.'api/mail.send.json';

  // Generate curl request
  $session = curl_init($request);
  curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);

  // Tell curl to use HTTP POST
  curl_setopt ($session, CURLOPT_POST, true);

  // Tell curl that this is the body of the POST
  curl_setopt ($session, CURLOPT_POSTFIELDS, $params);

  // Tell curl not to return headers, but do return the response
  curl_setopt($session, CURLOPT_HEADER, false);
  curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

  // obtain response
  $response = curl_exec($session);
  curl_close($session);
} 
?>
<!DOCTYPE html>
<html lang="en">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko"  xml:lang="ko">
  <head>
    <meta charset="utf-8">
	  <meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
    <meta name="dcterms.created" content="Mon, 18 Apr 2016 17:20:21 GMT">
    <meta name="description" content="Voting page for KISEM">
    <meta name="keywords" content="KISEM">
	
    <title>KISEM Vote</title>
    
    <style type="text/css">

    body {
      color:#000000;
      background-color:#FFFFFF;
    }
    a  { color:#0000FF; }
    a:visited { color:#800080; }
    a:hover { color:#008000; }
    a:active { color:#FF0000; }
	h1 {
	   font-size:50px; 
	}
  .footer, .push {
    position:absolute;
    width: 100%;
    margin-bottom:0;
    background-color:black;
    text-align:center;
    padding-top:40px;
    color: #cc8a00;
    height: 100px;
    border-top: 10px solid #cc8a00;
  }
  .chart div {
  font: 30px sans-serif;
  text-align: right;
  padding: 3px;
  margin: 1px;
  color: white;
  }

    </style>
    <!--[if IE]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>

  <body>
  	<p style="max-width:1280px; z-index: 2; margin: 0 auto !important; padding-bottom:30px;"><img src="https://reetzyplusimg.blob.core.windows.net/pub/kisem_logo.jpg" alt="KISEM Logo" style='width:100%;'></p>
	<hr>
  		<h1 style="text-align:center; color:#002878">Vote Results</h1>
	<br>
  <br>
		<?php
      $syes = $yes*5;
      $sno = $no*5;

		?>
  <div class="result">
      <p style="text-align:center; font-size:27px; padding-bottom:2px"><b>YES</b>: <?php echo $yes ?>&nbsp&nbsp&nbsp&nbsp<b>NO</b>: <?php echo $no ?></p>         
      <p style="text-align:center; font-size:27px; color:#cc8a00">Total: <?php echo $tot ?></p>
  </div>


  <div class="chart">
      <div style="width: <?php echo $syes?>px; background-color:green; color:black">YES (<?php echo $yes ?>)</div>
      <div style="width: <?php echo $sno?>px; background-color:red; ; color:black">NO (<?php echo $no ?>)</div>
  </div>
	<br>
	<br>
  </body>
  <p style="text-align:center; font-size:20px; color:red">**Please do not vote more than once. Your IP address is being checked.</p>
  <p style="text-align:center; font-size:20px; color:red"><?php echo "Your IP is ".$ipip ?></p>
  <br>
  <br>
  <br>
  <br>

	<div class="footer">
       <p>&copy; 2016 Jaeyoon (Brian) Kim. All Rights Reserved.</p>
  </div>
</html>




